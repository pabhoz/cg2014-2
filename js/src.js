var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var objetos = {};
var appW = window.innerWidth;
var appH = window.innerHeight;
var clock;
var TECLA = { ARRIBA:false, ABAJO:false, IZQUIERDA:false, DERECHA:false};
var cargador = {
	loadState: false,
	objsToLoad: 1,
	objsLoaded: 0,
	sceneIsReady: false,
	objReady: function(){
		this.objsToLoad--;
		this.objsLoaded++;
		if(this.objsToLoad == 0){
			this.loadState = true;
		}
	}
};

function webGLStart(){

	clock = new THREE.Clock;

	iniciarEscena();
	ultiTiempo = Date.now();
	document.onkeydown = teclaPulsada;
	document.onkeyup = teclaSoltada;
	animarEscena();
}

function iniciarEscena(){
	
	//RENDERER
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize( appW , appH);
	renderer.setClearColorHex( 0xAAAAAA, 1.0);

	renderer.shadowMapEnabled = true;

	document.body.appendChild(renderer.domElement);

	//CAMARAS
	var view_angle = 45, aspect_ratio = appW/appH, near = 1, far = 20000;
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far);
	camera.position.set( 0,150,400 );

	//CONTROL DE LA CAMARA
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement);

	//ESTADÍSTICAS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

	//Luces
	luzSpotLight = new THREE.SpotLight(0xffffff);
	luzSpotLight.position.set(0,25,0);

	bombilla = new THREE.Mesh(new THREE.SphereGeometry(1,1,1),new THREE.MeshBasicMaterial({color:0xffff00, wireframe:true}));

	//TERRENO
	var geometry = new THREE.PlaneGeometry(100,100,3,3);

	var material = new THREE.MeshLambertMaterial({color:0x8a8a8a, overdraw:true, side: THREE.DoubleSide, wireframe:true});

	mojojojo = new THREE.Mesh(geometry,material);
	mojojojo.rotation.x = -Math.PI/2;
	mojojojo.position.set(0,0,0);

	//Cubo
	var geometry = new THREE.CubeGeometry(5,5,10,1,1,1);
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color:0x00ff00});
	var wireframe = new THREE.MeshBasicMaterial({color:0x000000,wireframe:true});
	materials.push(material);
	materials.push(wireframe);
	leCarro = THREE.SceneUtils.createMultiMaterialObject(geometry,materials);
	leCarro.position.set(0,2.5,0);

	//LeCamaro

	materialCamaro = {
		body: {
			Red: new THREE.MeshPhongMaterial({
				color: 0x660000,
				combine: THREE.MixOperation,
				reflectivity: 0.3,
				shininess: 200
			}),
			Blue: new THREE.MeshPhongMaterial({
				color: 0x226699,
				combine: THREE.MixOperation,
				reflectivity: 0.3,
				shininess: 200
			})
		},
		chrome: new THREE.MeshLambertMaterial({
			color:0xffffff
		}),
		darkchrome: new THREE.MeshLambertMaterial({
			color: 0x444444
		}),
		glass: new THREE.MeshBasicMaterial({
			color: 0x223344,
			opacity: 0.25,
			combine: THREE.MixOperation,
			reflectivity: 0.25,
			transparent: true
		}),
		tire: new THREE.MeshLambertMaterial({
			color: 0x050505
		}),
		interior: new THREE.MeshPhongMaterial({
			color: 0x050505,
			shininess: 20
		}),
		black: new THREE.MeshLambertMaterial({
			color: 0x000000
		})
	}

	var loader = new THREE.BinaryLoader();

	loader.load("models/camaro/CamaroNoUV_bin.js",function(geometry){
		
		var m = new THREE.MeshFaceMaterial();

		m.materials[0] = materialCamaro.body["Red"]; //Cuerpo del carro
		m.materials[1] = materialCamaro.chrome; // Cromo de las llantas
		m.materials[2] = materialCamaro.chrome; // grille chrome
		m.materials[3] = materialCamaro.darkchrome; // door lines
        m.materials[4] = materialCamaro.glass; // windshield
        m.materials[5] = materialCamaro.interior; // interior
        m.materials[6] = materialCamaro.tire; // tire
        m.materials[7] = materialCamaro.black; // tireling
        m.materials[8] = materialCamaro.black; // behind grille

        camaro = new THREE.Mesh(geometry,m);
        cargador.objReady();
        
    });

}

function llenarEscena() {
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x808080, 1000, 6000 );
	scene.add( camera );
	camera.lookAt(scene.position);

	// Luces
	scene.add(new THREE.AmbientLight(0x020202));
	scene.add(luzSpotLight);
	bombilla.position = luzSpotLight.position;
	scene.add(bombilla);

	//Elementos
	scene.add(mojojojo);
	scene.add(camaro);
	//scene.add(leCarro);
}

function animarEscena(){
	requestAnimationFrame( animarEscena );

	if(!cargador.loadState){
		console.log("Obj Loaded : "+cargador.objsLoaded+" / "+(cargador.objsToLoad+cargador.objsLoaded));
	}else{
		if(!cargador.sceneIsReady){
			llenarEscena();
			console.log("Obj Loaded: "+cargador.objsLoaded+" from "+(cargador.objsToLoad+cargador.objsLoaded));
			console.log("scene Ready");
			cargador.sceneIsReady = true;

		}
		renderEscena();
		actualizarEscena();
	}
}

function renderEscena(){
	renderer.render( scene, camera );
}

function actualizarEscena(){
	
	ultiTiempo = Date.now();
	controlCamara.update();

	var time = Date.now() *0.0005;
	var delta = clock.getDelta();
	
	var movSpeed = 1;
	var velocidad = 0;
	var manejabilidad = 0.03;

	if(TECLA.ARRIBA){
		velocidad += (movSpeed*delta);
		moveObject(camaro , new THREE.Vector3(0,0,1) , movSpeed);
		console.log("yay!.");
	}else{
		velocidad = 0;
	}
	if(TECLA.ABAJO){
		moveObject(camaro , new THREE.Vector3(0,0,1) , -movSpeed/2);

	}
	if(TECLA.IZQUIERDA){
		rotateAroundWorldAxis(camaro , new THREE.Vector3(0,1,0) , manejabilidad);

	}
	if(TECLA.DERECHA){
		rotateAroundWorldAxis(camaro , new THREE.Vector3(0,1,0) , -manejabilidad);
	}

	stats.update();


}